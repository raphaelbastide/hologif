# Hologif

Turn any gif into an “hologram”.

This simple HTML page will display a gif animation (or any other web element) four times in cardinal directions.  
This is useful if you want to make *[3D hologram with smartphones or tablets](https://www.youtube.com/results?search_query=hologram%20smartphone)*.

## Demo

![Demo animation](animation.gif)

## Resources

- [How to make a hologram projector with CD cases](https://www.youtube.com/watch?v=JqyK7CNZx2g#t=14)
- [Ready made cardinal animations video](https://www.youtube.com/watch?v=asNoWcrfebk)

## Notes

- Dark or transparent background animated gif are best for an optimal result.
- You can adjust images distance by changing the `top` of `.face img` in the CSS.

## TODO

- UI: image distance and size could be chosen with a slider

## License

[wtfpl](http://www.wtfpl.net/txt/copying/)
